//1. Опишіть своїми словами різницю між функціями setTimeout() і setInterval().
// - setTimeout дає змогу викликати функцію один раз через певний інтервал часу.
// - setInterval дає змогу викликати функцію регулярно, повторюючи виклик через певний інтервал часу.
//
//2. Що станеться, якщо в функцію setTimeout() передати нульову затримку? Чи спрацює вона миттєво і чому?
// Якщо викликати функцію setTimeout() з нулевою затримкою, то вона буде викликана тільки після завершення виконання поточного коду.
//
//3. Чому важливо не забувати викликати функцію clearInterval(), коли раніше створений цикл запуску вам вже не потрібен?
// Функція очищає пам'ять та відміняє регулярне виконання функції setInterval(), коли її цикл більше не потрібен.

let images = [...document.querySelectorAll("img")];
let resumeButton = document.querySelector(".resumeButton");
let stopButton = document.querySelector(".stopButton");


let currentImg = 0;
let prevImg = 0;
let intervalId = setInterval(switchImages,3000);

function switchImages(){
    images[prevImg].classList.remove("image-to-show-active");
    images[prevImg].classList.add("image-to-show");
    currentImg++;
    if(currentImg > images.length - 1) currentImg = 0;
    images[currentImg].classList.remove("image-to-show");
    images[prevImg].classList.add("image-to-show-active");
    prevImg = currentImg;
}

function resume() {
    let intervalId = setInterval(switchImages, 3000);
}

function stop() {
        clearInterval(intervalId);
}

resumeButton.addEventListener("click", resume);
stopButton.addEventListener("click", stop);
